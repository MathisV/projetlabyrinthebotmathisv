#include <stdio.h>
#include <stdlib.h>

int main(){
    int two = 2;
    int *pointeur = &two; 
    int value = 1;
    printf("\npointeur = %p",pointeur);
    printf("\n*pointeur = %d",*pointeur);
    printf("\n&pointeur = %p",&pointeur);
    printf("\n&value = %p",&value);
    return 0;
}