#include <stdio.h>
#include "math.h"
#include "labyParse.h"
#include "BotDeSeptLieues.h"

int * direction(int d, int coord[2],int size_x,int size_y){
    if(d==0){
        //printf("La case nord la plus proche est : (%d, %d) \n",coord[0],coord[1]-1);
        coord[1]=coord[1]-1;
    }else if(d==1){
        //printf("La case Est la plus proche est : (%d, %d) \n",coord[0]+1,coord[1]);
        coord[0]=coord[0]+1;
    }else if(d==2){
        //printf("La case sud la plus proche est : (%d, %d) \n",coord[0],coord[1]+1);
        coord[1]=coord[1]+1;
    }else{
        //printf("La case ouest la plus proche est : (%d, %d) \n",coord[0]-1,coord[1]);
        coord[0]=coord[0]-1;
    }
    if(coord[0]<0){
        coord[0] = 0;
    }
    if(coord[1]<0){
        coord[1] = 0;
    }
    if(coord[0]>size_x-1){
        coord[0]=size_x-1;
    }
    if(coord[1]>size_y-1){
        coord[1]=size_y-1;
    }
    return coord;
}//Si on dépasse les bordures on aura x négatif ou bien x>size_x etc...

int CanIPass(t_tile T1, t_tile T0, int X1, int Y1,int X0, int Y0,int size_x,int size_y){
    //printf("\nOn compare la case (%d,%d) et (%d,%d) :",X0,Y0,X1,Y1);
    if(Y1==Y0+1){
        if(!T1.N && !T0.S){
            return 1;
        }else{
            return 0;
        }
    }
    if(Y1+1==Y0){
        if(!T1.S && !T0.N){
            return 1;
        }else{
            return 0;
        }
    }
    if(X1==X0+1){
        if(!T1.W && !T0.E){
            return 1;
        }else{
            return 0;
        }
    }
    if(X1+1==X0){
        if(!T1.E && !T0.W){
            return 1;
        }else{
            return 0;
        }
    }
    if(X1==X0 && Y1==Y0){
        //printf("\nDeux fois la même case.");
        return 0;
    }
    //int BoolEdgesX = ((X1==size_x&&X2==0)||(X2==size_x&&X1==0));
    //int BoolEdgesY = ((Y1==size_y&&Y2==0)||(X1==size_y&&X1==0));
    printf("\nError CanIPass");
    return 0;
}

int afficheRoute(int size_x, int size_y, int laby[size_x][size_y], int depart[2], int arrivee[2]){
    for(int i = 0; i < size_y; i++){
        printf("\n");
        for(int j = 0; j < size_x; j++){
            if(laby[j][i] == -1){
                printf("█");
            }else if(i==depart[1] && j==depart[0]){
                printf("▒");
            }else if(i==arrivee[1] && j==arrivee[0]){
                printf("⚑");
            }else if(laby[j][i] == 0){
                printf(" ");
            }else if(laby[j][i]>0){//Pour l'affichage console, on affiche en différente couleur les unités dépassants 10.
                int dizaine = (laby[j][i] - laby[j][i]%10)/10;
                if(dizaine%7 == 1){ 
                    printf("\033[0;31m");
                    printf("%d",(laby[j][i])%10);
                    printf("\033[0m");
                }else if(dizaine%7 == 2){ 
                    printf("\033[0;32m");
                    printf("%d",(laby[j][i])%10);
                    printf("\033[0m");
                }else if(dizaine%7 == 3){
                    printf("\033[0;33m");
                    printf("%d",(laby[j][i])%10);
                    printf("\033[0m");
                }else if(dizaine%7 == 4){ 
                    printf("\033[0;34m");
                    printf("%d",(laby[j][i])%10);
                    printf("\033[0m");
                }else if(dizaine%7 == 5){ 
                    printf("\033[0;35m");
                    printf("%d",(laby[j][i])%10);
                    printf("\033[0m");
                }else if(dizaine%7 == 6){ 
                    printf("\033[0;36m");
                    printf("%d",(laby[j][i])%10);
                    printf("\033[0m");
                }else{
                    printf("%d",(laby[j][i])%10);
                }
            }else{
                printf("\033[0;31m");
                printf("▒");
                printf("\033[0m");
            }
        }
    }
    printf("\n");
    return 0;
}

int FindRoute(int size_x, int size_y,t_tile laby[size_x][size_y],int Item,t_player player){
    int tab[size_x][size_y];
    for(int y = 0; y < size_y; y++){ //On initialise un tableau des distances
        for(int x = 0; x < size_x; x++){
            tab[x][y]=-1;}}
    int r = 0;
    int Test_impossibilite = 0;
    int arrivee[2]= {0,0};
    int coord[2] = {player.x,player.y};
    getItemPos(size_x,size_y,laby,Item,&arrivee[0],&arrivee[1]);
    //printf("\narrivee[0] = %d,arrivee[1] = %d",arrivee[0],arrivee[1]);
    tab[player.x][player.y]=1;
    //printf("\nr = %d, player.x = %d, player.y = %d, tab[player.x][player.y] = %d, tab[arrivee[0]][arrivee[1]] = %d",r,player.x,player.y,tab[player.x][player.y],tab[arrivee[0]][arrivee[1]]);
    while(tab[arrivee[0]][arrivee[1]]==-1){
        r++;
        int depart[2]={player.x,player.y};
        //afficheRoute(size_x, size_y, tab, depart, arrivee);
        Test_impossibilite = 0;
        for(int y = 0; y < size_y; y++){
            //printf("\ny = %d ",y);
            for(int x = 0; x < size_x; x++){  //On parcourt tout le tableau
                //printf("\n x = %d ",x);
                if(tab[x][y]==r){   //Si une case possède la distance max.
                    //printf("\nr = %d : Case r détéctée en (%d,%d)  \n",r,x,y);
                    Test_impossibilite++;
                    coord[0] = x;
                    coord[1] = y; //On se place sur cette case.
                    for(int d = 0; d < 4; d++){ 
                        direction(d,coord,size_x,size_y);      //On se place sur chaque case voisine
                        if(CanIPass(laby[coord[0]][coord[1]],laby[x][y],coord[0],coord[1],x,y,size_x,size_y) && tab[coord[0]][coord[1]]==-1){ //Si une case voisine n'a pas été traversée :
                            //printf("\nr = %d : Case voisine détéctée en (%d,%d) ",r,coord[0],coord[1]);
                            tab[coord[0]][coord[1]]=r+1; //On lui donne la distance r+1
                        }
                        coord[0] = x; //On retourne sur la case parcourue
                        coord[1] = y;
                    }
                }
            }
        }
        if(Test_impossibilite == 0){
            //printf("\n Chemin impossible :( \n");
            return 0;
        }
    }
    //printf("\n Le chemin est possible et mesure %d cases ! \n",tab[arrivee[0]][arrivee[1]]);
    return 1;
}

void Place_The_Item(int size_x, int size_y,t_tile laby[size_x][size_y],int Item,t_player player,t_move playersMove,t_tile extTile,t_move *botMove,t_move opponentMove){

}

int Test_All_Moves(int size_x, int size_y,t_tile laby[size_x][size_y],int Item,t_player player,t_player opponent,t_move playersMove,t_tile extTile,t_move *botMove,t_move opponentMove){
    //Test en x :
    t_player playerBis = player;
    t_player opponentBis = opponent;
    t_tile extTileBis = extTile; 
    t_tile Laby2[size_x][size_y];
    for(int y = 0; y < size_y; y++){ //On initialise un tableau des distances
        for(int x = 0; x < size_x; x++){
            Laby2[x][y]=laby[x][y];}}
    int ValidCol = floor((size_x-2)/2);
    int ValidRow = floor((size_y-2)/2);
    int x0 = player.x;
    int y0 = player.y;
    t_move move = playersMove;
    int posItem[2];
    if(extTile.Item!=player.nextItem){ //Si l'item n'est pas sur la case à poser. 
        getItemPos(size_x,size_y,laby,Item,&posItem[0],&posItem[1]);
        for(int rot=0;rot<4;rot++){
            move.rotation = rot;
            for(int j=2;j<4;j++){
                move.insert = j;
                for(int i=0;i<ValidCol;i++){
                    move.number = 2*i+1;
                    if(!((move.insert==2 && posItem[1]==size_y-1 && posItem[0]==move.number)||(move.insert==3 && posItem[1]==0 && posItem[0]==move.number))){
                        //Vérifie que le move ne push pas l'item cherché hors du plateau.
                        updateLaby(move,size_x,size_y,Laby2,&playerBis,&opponentBis,&extTileBis,x0,y0); 
                        if(FindRoute(size_x,size_y,Laby2,player.nextItem,playerBis)){
                            printf("\nCoup Trouvé ! Pour insert = %d et number = %d\n",j,2*i+1);
                            botMove->rotation = rot;
                            botMove->insert = j;
                            botMove->number = 2*i+1;
                            if(!((opponentMove.insert==0 && botMove->insert==1)||(opponentMove.insert==1 && botMove->insert==0)||(opponentMove.insert==3 && botMove->insert==2)||(opponentMove.insert==2 && botMove->insert==3))){
                                //Vérifie si le move n'est pas l'inverse de celui joué par l'adversaire.
                                getItemPos(size_x,size_y,laby,player.nextItem,&botMove->x,&botMove->y);
                                //Place botMove.x et y sur le trésor.
                                return 1;
                            }else{
                                printf("\nIllegal Move Aborted Col");
                            }
                        }
                    }else{
                        printf("\nAborted Pushing Item out of the board");
                    }
                    playerBis = player;
                    opponentBis = opponent;
                    extTileBis = extTile; 
                    for(int y = 0; y < size_y; y++){ //On initialise un tableau des distances
                        for(int x = 0; x < size_x; x++){
                            Laby2[x][y]=laby[x][y];}}
                }
            }
            for(int j=0;j<2;j++){
                move.insert = j;
                for(int i=0;i<ValidRow;i++){
                    move.number = 2*i+1;
                    if(!((move.insert==0 && posItem[0]==size_x-1 && posItem[1]==move.number)||(move.insert==1 && posItem[0]==0 && posItem[1]==move.number))){
                        //Vérifie que le move ne push pas l'item cherché hors du plateau.
                        updateLaby(move,size_x,size_y,Laby2,&playerBis,&opponentBis,&extTileBis,x0,y0); 
                        if(FindRoute(size_x,size_y,Laby2,player.nextItem,playerBis)){
                            printf("\nCoup Trouvé ! Pour insert = %d et number = %d\n",j,2*i+1);
                            botMove->rotation = rot;
                            botMove->insert = j;
                            botMove->number = 2*i+1;
                            if(!((opponentMove.insert==0 && botMove->insert==1)||(opponentMove.insert==1 && botMove->insert==0)||(opponentMove.insert==3 && botMove->insert==2)||(opponentMove.insert==2 && botMove->insert==3))){
                                //Vérifie si le move n'est pas l'inverse de celui joué par l'adversaire.
                                getItemPos(size_x,size_y,laby,player.nextItem,&botMove->x,&botMove->y);
                                //Place botMove.x et y sur le trésor.
                                return 1;
                            }else{
                                printf("\nIllegal Move Aborted Row");
                            }
                        }
                    }else{
                        printf("\nAborted Pushing Item out of the board");
                    }
                    playerBis = player;
                    opponentBis = opponent;
                    extTileBis = extTile; 
                    for(int y = 0; y < size_y; y++){ //On initialise un tableau des distances
                        for(int x = 0; x < size_x; x++){
                            Laby2[x][y]=laby[x][y];}}
                }
            }
        }
    }
    else{   //Si l'item à chercher se trouve la case à poser : 
        printf("\nL'item se trouve sur la case extérieure !!");
        for(int rot=0;rot<4;rot++){
            move.rotation = rot;
            for(int j=2;j<4;j++){
                move.insert = j;
                for(int i=0;i<ValidCol;i++){
                    move.number = 2*i+1;
                    updateLaby(move,size_x,size_y,Laby2,&playerBis,&opponentBis,&extTileBis,x0,y0); 
                    if(FindRoute(size_x,size_y,Laby2,player.nextItem,playerBis)){
                        printf("\nCoup Trouvé ! Pour insert = %d et number = %d\n",j,2*i+1);
                        botMove->rotation = rot;
                        botMove->insert = j;
                        botMove->number = 2*i+1;
                        if(!((opponentMove.insert==0 && botMove->insert==1)||(opponentMove.insert==1 && botMove->insert==0)||(opponentMove.insert==3 && botMove->insert==2)||(opponentMove.insert==2 && botMove->insert==3))){
                            //Vérifie si le move n'est pas l'inverse de celui joué par l'adversaire.
                            getItemPos(size_x,size_y,Laby2,Item,&botMove->x,&botMove->y);
                            //Place botMove.x et y sur la nouvelle case du trésor.
                            return 1;
                        }else{
                            printf("\nIllegal Move Aborted Col");
                        }
                    }
                    playerBis = player;
                    opponentBis = opponent;
                    extTileBis = extTile; 
                    for(int y = 0; y < size_y; y++){ //On initialise un tableau des distances
                        for(int x = 0; x < size_x; x++){
                            Laby2[x][y]=laby[x][y];}}
                }
            }
            for(int j=0;j<2;j++){
                move.insert = j;
                for(int i=0;i<ValidRow;i++){
                    move.number = 2*i+1;
                    updateLaby(move,size_x,size_y,Laby2,&playerBis,&opponentBis,&extTileBis,x0,y0); 
                    if(FindRoute(size_x,size_y,Laby2,player.nextItem,playerBis)){
                        printf("\nCoup Trouvé ! Pour insert = %d et number = %d\n",j,2*i+1);
                        botMove->rotation = rot;
                        botMove->insert = j;
                        botMove->number = 2*i+1;
                        if(!((opponentMove.insert==0 && botMove->insert==1)||(opponentMove.insert==1 && botMove->insert==0)||(opponentMove.insert==3 && botMove->insert==2)||(opponentMove.insert==2 && botMove->insert==3))){
                            //Vérifie si le move n'est pas l'inverse de celui joué par l'adversaire.
                            getItemPos(size_x,size_y,Laby2,Item,&botMove->x,&botMove->y);
                            //Place botMove.x et y sur la nouvelle case du trésor.
                            return 1;
                        }else{
                            printf("\nIllegal Move Aborted Row");
                        }
                    }
                    playerBis = player;
                    opponentBis = opponent;
                    extTileBis = extTile; 
                    for(int y = 0; y < size_y; y++){ //On initialise un tableau des distances
                        for(int x = 0; x < size_x; x++){
                            Laby2[x][y]=laby[x][y];}}
                }
            }
        }
    }
    if(opponentMove.insert == 3 || opponentMove.insert == 2){
        botMove->insert=1;
        botMove->x=player.x;
        botMove->y=player.y;
        botMove->number = 1;
        botMove->rotation = 0;
    }else{
        botMove->insert=3;
        botMove->x=player.x;
        botMove->y=player.y;
        botMove->number = 1;
        botMove->rotation = 0;
    }
    printf("\nAucun coup intéressant trouvé, coup aléatoire joué :( ");
    return 0;
}

/*

int CanIPass(t_tile T1, t_tile T2, int X1, int Y1,int X2, int Y2,int size_x,int size_y){
    printf("\nOn compare la case (%d,%d) et (%d,%d) :",X2,Y2,X1,Y1);
    if(Y1==Y2+1 || (Y1==size_y && Y2==0)){
        if(!T1.N && !T2.S){
            return 1;
        }else{
            return 0;
        }
    }
    if(Y1+1==Y2 || (Y2==size_y && Y1==0)){
        if(!T1.S && !T2.N){
            return 1;
        }else{
            return 0;
        }
    }
    if(X1==X2+1 || (X1==size_y && X2==0)){
        if(!T1.E && !T2.W){
            return 1;
        }else{
            return 0;
        }
    }
    if(X1+1==X2 || (X2==size_y && X1==0)){
        if(!T1.W && !T2.E){
            return 1;
        }else{
            return 0;
        }
    }
    if(X1==X2 && Y1==Y2){
        printf("\nDeux fois la même case.");
        return 0;
    }
    //int BoolEdgesX = ((X1==size_x&&X2==0)||(X2==size_x&&X1==0));
    //int BoolEdgesY = ((Y1==size_y&&Y2==0)||(X1==size_y&&X1==0));
    printf("\nError CanIPass");
    return 0;
}

*/