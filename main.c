#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "labyrinthAPI.h"
#include "labyParse.h"
#include "BotDeSeptLieues.h"

//seed swagg : f09537f7eb6d


int runGame(){
    connectToServer("172.105.76.204",1234,"BotDeSepttLieues15");
    int sizeX,sizeY,tileN,tileW,tileS,tileE,tileItem,IsWinning;
    int x0,y0 = 0;
    char name[50];
    waitForLabyrinth("TRAINING RANDOM timeout=1000 seed=0x3b39f4",name,&sizeX,&sizeY);    
    printf("nom laby = %s \n",name);
    int *rawLaby;
    int *tempLaby;
    rawLaby = malloc(sizeof(int)*5*sizeX*sizeY);
    tempLaby = malloc(sizeof(int)*5*sizeX*sizeY);
    int turn = getLabyrinth(tempLaby, &tileN, &tileE, &tileS, &tileW, &tileItem);
    t_tile extTile;
    int tabExtTile[5] = {tileN,tileE,tileS,tileW,tileItem};
    IntToTile(tabExtTile,&extTile,0);
    t_player player;
    t_player opponent;
    player.x = 0;
    player.y = 0;
    opponent.x = 0;
    opponent.y = 0;
    if(turn == 0){
        player.nextItem = 1;
    }else{
        opponent.nextItem = 24;
    }
    //printTempBoardRaw(sizeX,sizeY,tempLaby);
    t_tile laby[sizeX][sizeY];
    getLaby(sizeX,sizeY,tempLaby,laby);
    printLaby(sizeX,sizeY,laby);
    printLabyrinth();
    t_move opponentMove;
    t_move playersMove;
    t_move botMove;
    opponentMove.tileN = extTile.N;
    opponentMove.tileE = extTile.E;
    opponentMove.tileS = extTile.S;
    opponentMove.tileW = extTile.W;
    opponentMove.tileItem = extTile.Item;
    playersMove.tileN = extTile.N;
    playersMove.tileE = extTile.E;
    playersMove.tileS = extTile.S;
    playersMove.tileW = extTile.W;
    playersMove.tileItem = extTile.Item;
    int tabl[5];
    while(1){
        printf("\n");
        printLabyrinth();
        //printLaby(sizeX,sizeY,laby);
        if(turn){
            playersMove.tileN = extTile.N;
            playersMove.tileE = extTile.E;
            playersMove.tileS = extTile.S;
            playersMove.tileW = extTile.W;
            playersMove.tileItem = extTile.Item;
            opponentMove.tileN = extTile.N;
            opponentMove.tileE = extTile.E;
            opponentMove.tileS = extTile.S;
            opponentMove.tileW = extTile.W;
            opponentMove.tileItem = extTile.Item;
            printf("\nTour adversaire :---------------------------------------------------------------------------------------------\n");
            IsWinning = getMove(&opponentMove);
            updateLaby(opponentMove,sizeX,sizeY,laby,&player,&opponent,&extTile,x0,y0);
            opponent.x = opponentMove.x;
            opponent.y = opponentMove.y;
            opponent.nextItem = opponentMove.nextItem;
            if(IsWinning == WINNING_MOVE){
                printf("\nL'adversaire a gagné, vous avez perdu ! \n");
                return 0;
            }else if(IsWinning == LOOSING_MOVE){
                printf("\nL'adversaire a joué un coup Illegal, vous avez gagné ! \n");
                return 1;
            }
            turn = 0;
        }else{
            printf("\nVotre Tour---------------------------------------------------------------------------------------------------\n ");
            copyMove(playersMove,&botMove);
            if(!Test_All_Moves(sizeX,sizeY,laby,player.nextItem,player,opponent,playersMove,extTile,&botMove,opponentMove)){
                //Si Test_All_Moves n'a pas trouvé de coup intéressant.
                botMove.x = player.x;
                botMove.y = player.y;
                //Partie de code pour jouer manuellement :
                /*
                printf("\nVotre tour : \n");
                printf("               Entrer un coup : ");
                printf("\n Entrer INSERT LINE LEFT, INSERT LINE RIGHT, INSERT COLUMN TOP ou INSERT COLUMN BOTTOM : \n");
                scanf("%d",&playersMove.insert);
                printf("\n Entrer un numéro de ligne ou de colonne : \n");
                scanf("%d",&playersMove.number);
                printf("\n Entrer un nombre de rotation de la pièce : \n");
                scanf("%d",&playersMove.rotation);
                printf("\n Entrer un x où déplacer le pion : \n");
                scanf("%d",&playersMove.x);
                printf("\n Entrer un y où déplacer le pion : \n");
                scanf("%d",&playersMove.y);
                updateLaby(playersMove,sizeX,sizeY,laby,&player,&opponent,&extTile,x0,y0);
                player.x = playersMove.x;
                player.y = playersMove.y;
                sendMove(&playersMove);
               */ 
            }
            updateLaby(botMove,sizeX,sizeY,laby,&player,&opponent,&extTile,x0,y0);
            player.x = botMove.x;
            player.y = botMove.y;
            //printf("\n On send le déplacement : (%d,%d)",botMove.x,botMove.y);
            IsWinning = sendMove(&botMove);
            player.nextItem = botMove.nextItem;
            //printf("\n player.nextItem = %d, playersMove.nextItem = %d",player.nextItem, playersMove.nextItem);
            //printf("\nCase à Update : \n");
            //printTile(extTile);
            if(IsWinning == WINNING_MOVE){
                printf("\nVous avez gagné ! \n");
                return 1;
            }else if(IsWinning == LOOSING_MOVE){
                printf("\nVous avez perdu ! \n");
                return 0;
            }
            turn = 1;
        }
        x0 = player.x;
        y0 = player.y;
    }
    closeConnection();
    free(tempLaby);
}

void testFunc(){
}

int main(){
    //closeConnection();
    //testFunc();
    runGame();
    int a = 0;
    a++;
    return a;
}



/*
            tabl[0] = playersMove.tileN;
            tabl[1] = playersMove.tileE;
            tabl[2] = playersMove.tileS;
            tabl[3] = playersMove.tileW;
            tabl[4] = playersMove.tileItem;
            IntToTile(tabl,&extTile,0);
*/