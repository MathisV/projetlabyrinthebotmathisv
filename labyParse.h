#ifndef __LABYPARSE__
#define __LABYPARSE__

#include "labyrinthAPI.h"

typedef struct
{
	int x;
	int y;
    int nextItem;
} t_player;

typedef struct
{
	int N,E,S,W;
	int Item;
} t_tile;

void printLaby(int size_x, int size_y, t_tile laby[size_x][size_y]);
//Affiche le labyrinthe interne.

void getLaby(int sizeX, int sizeY,int tempLaby[sizeX*sizeY*5],t_tile laby[sizeX][sizeY]);

void printTile(t_tile Tile);

void IntToTile(int *tuile, t_tile *tile, int index);
//Prend les 5 entiers qui définissent une tuile et renvoie un t_tile.

int getItemPos(int size_x, int size_y,t_tile laby[size_x][size_y],int Item,int* PosX,int* PosY);
//Renvoie la position d'un item dans un labyrinthe.

void rotateTile(int rotation, t_tile *tile);
//Tourne une tuile.

void updateLaby(t_move move, int size_x, int size_y, t_tile laby[size_x][size_y], t_player *player,t_player *opponent, t_tile *extTile,int x0,int y0);

void copyMove(t_move move1, t_move *move2);
#endif

/*
void IntToTile(int *tuile, t_tile *tile);
//Prend les 5 entiers qui définissent une tuile et renvoie un t_tile.

void getLaby(int sizeX, int sizeY,int tempBoard[sizeX*sizeY][5],t_tile laby[sizeX][sizeY]);
//Prend un tableau de int et revoit un tableau de t_tile.

//void ArrayModuloFive(int sizeX, int sizeY, int *rawLaby, int *tempLaby);

void printTempBoardRaw(int sizeX, int sizeY,int tempBoard[sizeX*sizeY][5]);
//Test si le tempBoard est bien conforme.

void printBoard(int size_x, int size_y,t_tile board[size_x][size_y], t_tile extraTile);
void createTile(int * tempTile, t_tile * Tile);
*/