#include <stdio.h>
#include "labyParse.h"
#include "labyrinthAPI.h"

void IntToTile(int *intTile, t_tile *tile, int index){
    tile->N = *(intTile+5*index);
    tile->E = *(intTile+5*index+1);
    tile->S = *(intTile+5*index+2);
    tile->W = *(intTile+5*index+3);
    tile->Item = *(intTile+5*index+4);
}

void printTile(t_tile tile){
    printf("%d%d%d%d%d",tile.N,tile.E,tile.S,tile.W,tile.Item);
}

void getLaby(int sizeX, int sizeY,int *tempLaby,t_tile laby[sizeX][sizeY]) {
    printf("\nGet Laby 2 :\n");
    int x,y = 0;
    for(int i = 0; i<sizeX*sizeY; i++){
        if(i%sizeX==0 && x!=0){
            y++;
            x=0;
        }
        //printf("\n i,x,y : %d,%d,%d",i,x,y);
        IntToTile(tempLaby,&laby[x][y],i);
        x++;
    }
}

void printLaby(int size_x, int size_y, t_tile laby[size_x][size_y]){
    printf("\nPrint interne laby :\n");
    for(int i = 0; i < size_y; i++){
        for(int j = 0; j < size_x; j++){  //On parcourt tout le tableau
            printTile(laby[j][i]);
            printf(" ");
        }
        printf("\n");
    }
}

int getItemPos(int size_x, int size_y,t_tile laby[size_x][size_y],int Item,int* PosX,int* PosY){
    for(int i =0;i<size_x;i++){
        for(int j =0;j<size_y;j++){
            if(laby[i][j].Item == Item){
                //printf("\nTrouvé : PosX = %d, PosY = %d",i,j);
                *PosX = i;
                *PosY = j;
                return 1;
            }
        }
    }

    printf("\n!!!!!!!!!!!!!!Error getItemPos :");
    printf("\n      Item à chercher : %d",Item);
    printf("\n      laby[i][j].Item : %d",laby[6][1].Item);
    printf("\n      Intern Laby : \n");
    printLaby(size_x, size_y, laby);
    return 0;
}

void rotateTile(int rotation, t_tile *tile){
    int temp=0;
    for(int j = 0; j<rotation;j++){
            temp = tile->N;
            tile->N = tile->W; 
            tile->W = tile->S; 
            tile->S = tile->E; 
            tile->E = temp;
    }
}

void updateLaby(t_move move, int size_x, int size_y, t_tile laby[size_x][size_y], t_player *player,t_player *opponent, t_tile *extTile,int x0,int y0){
    t_tile extraTile;
    extraTile.N = extTile->N;
    extraTile.E = extTile->E;
    extraTile.S = extTile->S;
    extraTile.W = extTile->W;
    extraTile.Item = extTile->Item;
    t_tile tempTile;
    rotateTile(move.rotation,&extraTile);
    if(move.insert == INSERT_LINE_RIGHT){
        tempTile = laby[0][move.number];
        for(int i = 0; i<size_x;i++){
            laby[i][move.number]=laby[i+1][move.number];
        }
        if(x0==0 && y0==move.number){ 
            player->x = size_x-1; 
        }else if(y0==move.number){
            player->x += -1; 
        }
        if(opponent->x==0 && opponent->y==move.number){
            opponent->x = size_x-1;
        }else if(opponent->y==move.number){
            opponent->x += -1; 
        }
        laby[size_x-1][move.number]=extraTile;
    }
    else if(move.insert == INSERT_LINE_LEFT){
        tempTile = laby[size_x-1][move.number];
        for(int i = 0; i<size_x;i++){
            laby[size_x-i-1][move.number]=laby[size_x-i-2][move.number];
        }
        if(x0==size_x-1 && y0==move.number){
            player->x = 0;
        }else if(y0==move.number){
            player->x += 1; 
        }
        if(opponent->x==size_x-1 && opponent->y==move.number){
            opponent->x = 0;
        }else if(opponent->y==move.number){
            opponent->x += 1; 
        }
        laby[0][move.number]=extraTile;
    }
    else if(move.insert == INSERT_COLUMN_BOTTOM){
        tempTile = laby[move.number][0];
        for(int i = 0; i<size_y;i++){
            laby[move.number][i]=laby[move.number][i+1];
        }
        //printf("\n      (x0,y0) = (%d,%d)",x0,y0);
        //printf("\n      move.number = %d",move.number);
        if(x0==move.number && y0==0){
            //printf("\nPlayer Pushed to bottom ! ! ! ! !");
            player->y = size_y-1;
        }else if(x0==move.number){
            player->y += -1; 
        }
        if(opponent->x==move.number && opponent->y==0){
            opponent->y = size_y-1;
        }else if(opponent->x==move.number){
            opponent->y += -1; 
        }
        laby[move.number][size_y-1]=extraTile;
    }
    else if(move.insert == INSERT_COLUMN_TOP){
        tempTile = laby[move.number][size_y-1];
        for(int i = 0; i<size_x;i++){
            laby[move.number][size_y-1-i]=laby[move.number][size_y-i-2];
        }
        //printf("\n      (x0,y0) = (%d,%d)",x0,y0);
        //printf("\n      move.number = %d",move.number);
        if(x0==move.number && y0==size_y){
            player->y = 0;
        }else if(x0==move.number){
            player->y += 1; 
        }
        if(opponent->x==move.number && opponent->y==size_y){
            opponent->y = 0;
        }else if(opponent->x==move.number){
            opponent->y += 1; 
        }
        laby[move.number][0]=extraTile;
    }
    *extTile = tempTile;
    //printf("\nUpdated player.(x,y) = (%d,%d)",player->x,player->y);
}


void copyMove(t_move move1, t_move *move2){
	move2->insert=move1.insert; 	
	move2->number=move1.number; 	
	move2->rotation=move1.rotation; 	
	move2->x=move1.x; 	
	move2->y=move1.y; 	
	move2->tileN=move1.tileN; 	
	move2->tileE=move1.tileE; 	
	move2->tileS=move1.tileS; 	
	move2->tileW=move1.tileW; 	
	move2->tileItem=move1.tileItem; 	
	move2->nextItem=move1.nextItem; 	
}









































/*

void getLaby(int sizeX, int sizeY,int tempLaby[sizeX*sizeY][5],t_tile laby[sizeX][sizeY]) {
    for(int y = 0; y < sizeY; y++) {
        for(int x = 0; x < sizeX; x++) {
            t_tile tile;
            IntToTile(tempLaby[x+sizeX*y],&tile);
            printTile(tile);
            //IntToTile(tempLaby,&tile);
            laby[x][y] = tile;
            tempLaby+=5;
        }
    }
}


void printLabRaw(int size_x, int size_y,t_tile board[size_x][size_y], t_tile extraTile) {
    for(int x = 0; x < size_x; x++) {
        for(int y = 0; y < size_y; y++) {
            printf("%d %d %d %d %d   ",board[x][y].N,board[x][y].E,board[x][y].S,board[x][y].W,board[x][y].T);
        }
        printf("\n");
    }
    printf("Extra tile : %d %d %d %d %d\n",extraTile.N,extraTile.E,extraTile.S,extraTile.W,extraTile.T);
}

void ArrayModuloFive(int sizeX, int sizeY, int *rawLaby, int *tempLaby){
    for(int i = 0; i<sizeX*sizeY; i++){
        for(int j = 0; j<5; j++){
            tempLaby[i][j]=rawLaby[i*5+j];
        }
    }
}

void IntToTile(int *tuile, t_tile *tile) {
    tile->N = tuile[0];
    tile->E = tuile[1];
    tile->S = tuile[2];
    tile->W = tuile[3];
    tile->Item = tuile[4];
}

void printTempBoardRaw(int sizeX, int sizeY,int tempBoard[sizeX*sizeY][5]){
    printf("\n\n tempLaby a cette gueule : ");
    for(int i = 0; i<sizeX*sizeY; i++){
        if(i%sizeX==0){
            printf("\n");
        }
        for(int j = 0; j<5;j++){
            printf("%d",tempBoard[i][j]);
        }
        printf(" ");
    }
}

*/