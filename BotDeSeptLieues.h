#ifndef __BOTDESEPTLIEUES__
#define __BOTDESEPTLIEUES__

#include "labyParse.h"

int * direction(int d, int coord[2],int size_x,int size_y);

int CanIPass(t_tile T1, t_tile T2, int X1, int Y1,int X2, int Y2,int size_x,int size_y);
//Retourne 1 si 2 tuiles sont passantes, 0 sinon.
int afficheRoute(int size_x, int size_y, int laby[size_x][size_y], int depart[2], int arrivee[2]);

int FindRoute(int size_x, int size_y,t_tile laby[size_x][size_y],int Item,t_player player);

int Test_All_Moves(int size_x, int size_y,t_tile laby[size_x][size_y],int Item,t_player player,t_player opponent,t_move playersMove,t_tile extTile,t_move *botMove, t_move opponentMove);

#endif
