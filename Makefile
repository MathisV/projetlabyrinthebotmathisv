# Modèle de fichier Makefile, à adapter pour le TP

# options de compilation
CC = gcc
CCFLAGS = -Wall
SRC = main.c labyrinthAPI.c clientAPI.c labyParse.c BotDeSeptLieues.c
OBJ = $(SRC:.c=.o)


# fichiers du projet

EXEC = main


# règle initiale
all: $(EXEC)

#dépendances
clientAPI.o: clientAPI.h
labyrinthAPI.o: clientAPI.h labyrinthAPI.h 
labyParse.o: labyParse.h labyrinthAPI.h
BotDeSeptLieues.o: BotDeSeptLieues.h labyParse.h 
main.o: labyrinthAPI.h labyParse.h BotDeSeptLieues.h

# règles de compilation	
%.o: %.c
	$(CC) $(CCFLAGS) -o $@ -c $<
	
#edition de lien 
$(EXEC): $(OBJ)
	$(CC) $(CCFLAGS) -o $@ $^ 
# règles supplémentaires
clean:
	rm -f *.o *.out