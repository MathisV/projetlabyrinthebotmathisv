
Le BotDeSeptLieues calcule à chaque tour, tous les placements de tuiles et rotations possibles et cherche si ce placement de tuile permet d'atteindre le trésor à trouver. S'il n'existe pas de tel chemin, le bot joue un coup presque aléatoire.
Malheureusement, ce bot ne fonctionne que pour des boards 7x7 étonnemment, et je n'ai pas trouvé le temps de debugger pour les autres cas.
Si j'avais trouvé plus de temps, j'aurais ajouté une partie de code pour trouver un meilleur coup en cas de chemin impossible. Par exemple, chercher à rapprocher le joueur du trésor en posant une tuile sur pour faire rapprocher le trésor, ou le joueur, ou éloigner l'adversaire de son trésor.

Le BotDeSeptLieues se compose de 2 fonctions interessantes : 
	-FindRoute, qui trouve pour un labyrinthe donné s'il existe un chemin entre un joueur et son trésor.
		Il peut être intéressant de décommenter la ligne 147, afin d'afficher à chaque tour de boucle "d'expansion" pour voir comment l'algorithme cherche dans le labyrinthe.
	-Test_All_Moves, qui utilise FindRoute sur chaque placement de tuile possible pour trouver un coup permettant d'atteindre le trésor.
	
	
